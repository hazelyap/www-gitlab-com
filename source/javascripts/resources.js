import $ from 'jquery';
import 'select2'; 

(function() {
  var ResourceFilter = {
    topic: '',
    type: '',
    solution: '',
    // Set English as default
    language: 'English',
    load_group: 1,
    tile_group_size: 9,
    $no_results_msg: [],
    $load_more_link: [],
    $resources: [],
    $tile_group: [],
    $filtered_resources: [],
    init: function() {
      this.$resources = $('.js-resource-tile');
      if (this.$resources.length > 0) {
        this.setup();
      }
    },
    setup: function() {
      this.$no_results_msg = $('.js-resources-no-results-msg');
      this.$tile_group = this.$resources.parent();
      this.$filtered_resources = this.$resources;
      $('select[name="resource_topic"]').on('change', this.topicChange).select2();
      $('select[name="resource_type"]').on('change', this.typeChange).select2();
      $('select[name="resource_language"]').on('change', this.languageChange).select2();
      $('select[name="resource_solution"]').on('change', this.solutionChange).select2();
      if ( this.$resources.length > 9 ) {
        this.setupLoadMore();
      }
      // Apply default filters
      this.filterResources();
    },
    setupLoadMore: function() {
      // Add load more link
      this.$load_more_link = $('<a href="#load-more" class="js-resources-load-more btn cta-btn ghost-accent">Load more</a>');
      this.$tile_group.after($('<div class="btn-group"/>').append(this.$load_more_link));

      // Enable load more link
      $(document).on('click', '.js-resources-load-more', this.loadMore.bind(this));

      // Hide tiles beyond initial group
      this.hideTilesForLoad();
    },
    hideTilesForLoad: function() {
      var tilesToShow = this.tile_group_size * this.load_group;
      this.$filtered_resources.removeClass('hidden');
      this.$filtered_resources.filter(function(i) {
        return i >= tilesToShow;
      }).addClass('hidden');

      if (this.$filtered_resources.length < tilesToShow) {
        this.$load_more_link.addClass('hidden');
      } else {
        this.$load_more_link.removeClass('hidden');
      }
    },
    loadMore: function(e) {
      e.preventDefault();
      this.load_group++;
      this.hideTilesForLoad();
    },
    topicChange: function() {
      var self = ResourceFilter;
      self.topic = this.value;
      self.resetLoadGroup();
      self.filterResources();
    },
    typeChange: function() {
      var self = ResourceFilter;
      self.type = this.value;
      self.resetLoadGroup();
      self.filterResources();
    },
    languageChange: function() {
      var self = ResourceFilter;
      self.language = this.value;
      self.resetLoadGroup();
      self.filterResources();
    },
    solutionChange: function() {
      var self = ResourceFilter;
      self.solution = this.value;
      self.resetLoadGroup();
      self.filterResources();
    },
    resetLoadGroup: function() {
      this.load_group = 1;
    },
    filterResources: function() {
      var selector = this.buildFilterSelector();
      this.$no_results_msg.addClass('hidden');
      if (selector === '') {
        // Show All
        this.$filtered_resources = this.$resources;
      } else {
        // Show Some
        this.$filtered_resources = this.$resources.filter(selector);
      }

      // Hide resources that didn't match filter
      this.$resources.not(this.$filtered_resources).addClass('hidden');
      if (this.$filtered_resources.length === 0) {
        this.$no_results_msg.removeClass('hidden');
      }
      this.hideTilesForLoad();
    },
    buildFilterSelector: function() {
      var selector = '';

      if (this.topic) {
        selector += '[data-resource-topics*="||' + this.topic + '||"]';
      }
      if (this.type) {
        selector += '[data-resource-type="' + this.type + '"]';
      }
      if (this.language) {
        selector += '[data-resource-language="' + this.language + '"]';
      }
      if (this.solution) {
        selector += '[data-resource-solutions*="||' + this.solution + '||"]';
      }

      return selector;
    }
  };
  $(ResourceFilter.init.bind(ResourceFilter));
})();